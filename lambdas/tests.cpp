#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "catch.hpp"

using namespace std;

struct Gadget
{
    string id;
    int usage_counter = 0;

    explicit Gadget(string id)
        : id {move(id)}
    {
        cout << "Gadget(" << this->id << " - " << this << ")\n";
    }

    Gadget(const Gadget& source)
        : id {source.id}
    {
        cout << "Gadget(cc: " << this->id << " - " << this << ")\n";
    }

    Gadget(Gadget&& source) noexcept
        : id {move(source.id)}
    {
        cout << "Gadget(mv: " << this->id << " - " << this << ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << this->id << " - " << this << ")\n";
    }

    void use()
    {
        ++usage_counter;
    }

    auto get_reporter()
    {
        return [*this] { cout << "Report from Gadget(" << id << ")\n"; };
    }
};

TEST_CASE("lambda expressions")
{
    function<void()> reporter;

    {
        Gadget g{"ipad"};
        reporter = g.get_reporter();
        reporter();
    }

    reporter();
}

namespace Cpp20
{
    template <typename Iter, typename Predicate>
    constexpr Iter find_if(Iter first, Iter last, Predicate pred)
    {
        bool is_found = false;
        Iter it = first;
        for(; it != last; ++it)
        {
            if (pred(*it))
            {
                is_found = true;
                break;
            }
        }
        if (!is_found)
            throw std::runtime_error("Not found");        
        return it;
    }
}

constexpr std::array<int, 64> create_array()
{
    auto square = [](int x) { return x * x; };

    constexpr std::array<int, square(8)> data = { square(1), square(5), square(3), square(9) };

    return data;
}

TEST_CASE("lambda expressions are implicitly constexpr")
{
    auto square = [](int x) { return x * x; };

    static_assert(square(8) == 64);

    int tab[square(10)];

    static_assert(size(tab) == 100);

    constexpr std::array<int, square(2)> data = { square(1), square(5), square(2), square(9) }; 

    static_assert(data[2] == 4);   

    constexpr auto value = *(Cpp20::find_if(begin(data), end(data), [](int x) { return x % 2 == 0; }));

    static_assert(value == 4);

    auto by_factor = [value](int arg) { return arg * value; };

    static_assert(by_factor(3) == 12);

    if constexpr(by_factor(20) > 30)
    {
        cout << "Larger than 30\n";
    }
}