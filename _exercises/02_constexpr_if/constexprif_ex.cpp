#include <iostream>
#include <list>
#include <vector>

#include "catch.hpp"

using namespace std;

struct GenericImpl
{
};

struct RandomAccessIteratorImpl
{
};

template <typename Iterator>
auto advance_it(Iterator& it, size_t n)
{
    using IteratorCategoryTag 
        = typename std::iterator_traits<Iterator>::iterator_category;
    
    if constexpr (is_same_v<IteratorCategoryTag, std::random_access_iterator_tag>)
    {
        it += n;
        return RandomAccessIteratorImpl{};
    }
    else
    {
        for(size_t i = 0; i < n; ++i)
            ++it;
        return GenericImpl{};
    }    
}

TEST_CASE("constexpr-if with iterator categories")
{
    SECTION("random_access_iterator")
    {
        vector<int> data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        auto it = data.begin();

        RandomAccessIteratorImpl impl = advance_it(it, 3);

        REQUIRE(*it == 4);
    }

    SECTION("input_iterator")
    {
        list<int> data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        auto it = data.begin();

        GenericImpl impl = advance_it(it, 3);

        REQUIRE(*it == 4);
    }
}
