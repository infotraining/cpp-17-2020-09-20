#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "catch.hpp"

using namespace std;

struct SimpleAggregate
{
    int a;
    double b;
    int data[3];
    std::string name;
};

TEST_CASE("aggregates")
{
    SimpleAggregate sa1{1, 3.14, {1, 2, 3}, {'a', 16}};
    SimpleAggregate sa2{3};
    SimpleAggregate sa3{};
}

struct Point : std::string
{
    int x, y;    

    auto get_coord() 
    {
        return tuple(x, y);
    }
};

TEST_CASE("since C++17")
{
    Point pt{{"pt"s}, 1, 2};

    auto [pos_x, pos_y] = pt.get_coord();
}

TEST_CASE("std::array is aggregate")
{
    std::array<int, 2> buffer{ 1, 2 };
}

struct WTF
{
    int value;

    WTF() = delete;
};

TEST_CASE("wtf")
{
    WTF wtf{};
}

struct ErrorCode
{
    int errc;
    const char* msg;
};

[[nodiscard]] ErrorCode open_file(const char* filename)
{
    return {13, "Error#13"};
}

TEST_CASE("nodiscard")
{
    [[maybe_unused]] auto dev_null = open_file("bad.name");

    static_assert(sizeof(int) == 3);
}
