#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

struct NoDefaultContstructible
{
    int value;

    NoDefaultContstructible(int v)
        : value {v}
    {
    }
};

TEST_CASE("variant")
{
    variant<int, double, string, vector<int>> v1;

    REQUIRE(holds_alternative<int>(v1));
    REQUIRE(get<int>(v1) == 0);

    SECTION("when first type in the list is not default constructible")
    {
        variant<monostate, NoDefaultContstructible, int, double> v2;
        REQUIRE(v2.index() == 0);
    }

    v1 = 3.14;
    v1 = "text"s;
    v1 = vector{1, 2, 3};

    REQUIRE(get<vector<int>>(v1) == vector{1, 2, 3});   
    REQUIRE_THROWS_AS(get<int>(v1), bad_variant_access);

    REQUIRE(get_if<int>(&v1) == nullptr);
    REQUIRE(get_if<vector<int>>(&v1) != nullptr);

    if (auto ptr = get_if<vector<int>>(&v1); ptr)
        ptr->resize(42);

    v1.emplace<string>(16, 'a');

    std::cout << get<string>(v1) << "\n";
}

TEST_CASE("variant with duplicates")
{
    variant<int, string, int> v = "text"s;

    v.emplace<0>(42);

    REQUIRE_THROWS_AS(get<2>(v), bad_variant_access);       
}

struct Value
{
    float v;

    Value(float v)
        : v {v}
    {
    }

    ~Value()
    {
        cout << "~Value(" << v << ")\n";
    }
};


struct Evil
{
    string v;

    Evil(string v) : v{std::move(v)}
    {}

    Evil(Evil&& other)
    {
        throw std::runtime_error("42");
    }
};

TEST_CASE("valueless variant")
{
    variant<Value, Evil> v{12.0f};

    try
    {
        v.emplace<Evil>(Evil{"evil"});
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }    

    REQUIRE(v.valueless_by_exception());
    REQUIRE(v.index() == variant_npos);
}

struct Printer
{
    void operator()(double x) const
    {
        cout << "double: " << x << "\n";
    }

    void operator()(const string& str) const
    {
        cout << "string: " << str << "\n";
    }

    template <typename T, typename = enable_if_t<is_integral_v<T>>>
    void operator()(const T& item) const
    {
        cout << typeid(item).name() << " - " << item << "\n";
    }

    template <typename T, typename = void, typename = enable_if_t<is_floating_point_v<T>>>
    void operator()(const T& item) const
    {
        cout << typeid(item).name() << " - " << item << "\n";
    }
};

TEST_CASE("visiting variants")
{
    variant<int, short, double, string, float> v{3.14f};

    Printer printer;
    std::visit(printer, v);

    v = "text"s;

    auto printer_closure = [](const auto& item) { cout << "visiting with lambda: " << item << "\n";};
    std::visit(printer_closure, v);
}

template <typename... Closures>
struct overload : Closures...
{
    using Closures::operator()...; // new C++17 feature
};

template <typename... Closures>
overload(Closures...) -> overload<Closures...>;

TEST_CASE("visitor")
{
    auto uber_printer = overload {
        [](int x) { cout << "int: " << x << "\n"; },
        [](auto x) { cout << "auto: " << x << "\n"; },
        [](const string& s) { cout << "string: " << s << "\n"; }        
    };

    variant<int, double, string, short> v{3.14};
    visit(uber_printer, v);
}

struct ErrorCode
{
    std::errc error_code;
    std::string message;
};

[[nodiscard]] variant<string, ErrorCode> load_from_torrent(const string& torrent_name)
{
    if (torrent_name == "unknown")
        return ErrorCode{errc::bad_file_descriptor, "Error#14"};
    
    return "Content of "s + torrent_name;
}

TEST_CASE("handling results or errors")
{
    visit(overload{
        [](const string& content) { cout << content << "\n"; },
        //[](const ErrorCode& ec) { cout << ec.message << "\n"; }
    }, load_from_torrent("unknown"));
}