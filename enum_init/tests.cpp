#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <cstddef>

#include "catch.hpp"

using namespace std;

enum class Coffee : uint8_t
{
    espresso,
    chemex,
    v60
};

TEST_CASE("init enum types")
{
    Coffee cf{1};    

    REQUIRE(cf == Coffee::chemex);
}

TEST_CASE("std::byte")
{
    std::byte b1{13};
    std::byte b2{65};

    b1  = ((b1<<2) & b2);

    b1 = std::byte{42};

    REQUIRE(b1 == b2);

    std::cout << std::to_integer<int>(b1) << "; " << std::to_integer<int>(b2) << "\n";
}

void foo1() {}
void foo2() noexcept {}

template <typename F1, typename F2>
void call_wrapper(F1 f1, F2 f2)
{
    f1();
    f2();
}

TEST_CASE("noexcept")
{
    static_assert(!is_same_v<decltype(foo1), decltype(foo2)>);

    //void(*fun_ptr)() noexcept = foo1;

    call_wrapper(foo1, foo2);
}