#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <any>
#include <optional>
#include <map>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    any anything;

    anything = 1;
    anything = 3.14;
    anything = "text"s;
    anything = vector{1, 2, 3};

    SECTION("any_cast")
    {
        auto vec = any_cast<vector<int>>(anything);

        REQUIRE(vec == vector{1, 2, 3});

        REQUIRE_THROWS_AS(any_cast<string>(anything), bad_any_cast);
    }

    SECTION("any_cast with pointer")
    {
        if (vector<int>* ptr_vec = any_cast<vector<int>>(&anything); ptr_vec)
            ptr_vec->at(0) = 42;

        REQUIRE(any_cast<vector<int>>(&anything) != nullptr);
        REQUIRE(any_cast<int>(&anything) == nullptr);
    }

    SECTION("type + RTTI")
    {
        const type_info& type_desc = anything.type();

        cout << "anything: " << type_desc.name() << "\n";

        REQUIRE(anything.type() == typeid(vector<int>));
    }

    anything.reset();
}

class KeyValueDictionary
{
    map<string, any> dict_;

public:
    optional<map<string, any>::iterator> insert(string key, any value)
    {
        auto [pos, was_inserted] = dict_.emplace(move(key), move(value));

        if (was_inserted)
            return pos;
        return nullopt;
    }

    template <typename T>
    T& at(const string& key)
    {
        T* value = any_cast<T>(&dict_.at(key));

        if (!value)
            throw bad_any_cast();

        return *value;
    }
};


TEST_CASE("KeyValueDictionary")
{
    KeyValueDictionary dict;

    dict.insert("name", "jan"s);
    dict.insert("age", 33);
    dict.insert("data", vector {1, 3, 4});

    REQUIRE(dict.at<int>("age") == 33);
    REQUIRE(dict.at<vector<int>>("data") == vector {1, 3, 4});
    REQUIRE_THROWS_AS(dict.at<int>("name"), bad_any_cast);
}

class TempMonitor
{
public:
    double get_temp() const
    {
        return 23.88;
    }
};

class Observer
{
public:
    virtual void update(const std::any& sender, const string& msg) = 0;
    virtual ~Observer() = default;
};

class Logger : public Observer
{
public:
    void update(const std::any& sender, const string& msg) override
    {
        TempMonitor* monitor = any_cast<TempMonitor>(&sender);
        if (monitor)
            monitor->get_temp();
    }
};