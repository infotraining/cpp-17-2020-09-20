#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <queue>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializer")
{
    vector vec = {1, 2, 3, 4, 42, 665, 423523 };
    
    if (auto pos = std::find(begin(vec), end(vec), 665); pos != end(vec))
    {
        std::cout << "Item has been found: " << *pos << "\n";
    }
    else
    {
        assert(pos == end(vec));
    }
}

void foo()
{}

TEST_CASE("if with mutex")
{
    queue<int> q;
    mutex mtx_q;

    {
        lock_guard lk{mtx_q};
        q.push(42);
    }

    SECTION("pop")
    {        
        if (lock_guard lk{mtx_q}; !q.empty())
        {
            auto value = q.front();
            q.pop();
        } // unlock na mutexie        
    }    
}
