#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <optional>
#include <charconv>

#include "catch.hpp"

using namespace std;

optional<int> to_int(string_view str)
{
    int result {};

    auto start = str.data();
    auto end = start + str.size();

    if (const auto& [pos, error_code] = from_chars(start, end, result); error_code != std::errc {} || pos != end)
    {
        return nullopt;
    }

    return result;
}

TEST_CASE("to_int")
{
    optional n = to_int("42");

    REQUIRE(n.has_value());
    REQUIRE(*n == 42);

    n = to_int("4ab");

    REQUIRE(n.has_value() == false);
}