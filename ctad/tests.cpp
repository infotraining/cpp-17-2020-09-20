#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <optional>
#include <functional>
#include <array>

#include "catch.hpp"

using namespace std;

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce3(T&& arg)
{
    puts(__PRETTY_FUNCTION__);

    auto x = std::forward<T>(arg);
}

void foo()
{
}

TEST_CASE("Template Argument Deduction - case 1")
{
    int x = 10;
    const int cx = 20;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];    

    [[maybe_unused]] auto ax1 = 10;
    deduce1(10);

    [[maybe_unused]] auto ax2 = cx;
    deduce1(cx);

    [[maybe_unused]] auto ax3 = ref_x;
    deduce1(ref_x);

    [[maybe_unused]] auto ax4 = cref_x; // int
    deduce1(cref_x);

    [[maybe_unused]] auto ax5 = tab;
    deduce1(tab);

    [[maybe_unused]] auto ax6 = foo;
    deduce1(foo);
}

TEST_CASE("Template Argument Deduction - case 2")
{
    int x = 10;
    const int cx = 20;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];    

    [[maybe_unused]] auto& ax1 = x;
    deduce2(x);

    [[maybe_unused]] auto& ax2 = cx; // const int&
    deduce2(cx);

    [[maybe_unused]] auto& ax3 = ref_x;
    deduce2(ref_x);

    [[maybe_unused]] auto& ax4 = cref_x; // int
    deduce2(cref_x);

    [[maybe_unused]] auto& ax5 = tab; // int(&)[10]
    deduce2(tab);

    [[maybe_unused]] auto& ax6 = foo;
    deduce2(foo);
}

TEST_CASE("case 3")
{
    int x = 10;

    deduce3(x);
    [[maybe_unused]]auto&& ax1 = x; // int&

    deduce3(10);
    [[maybe_unused]] auto&& ax2 = 10; // int&&

    int&& rvref = 10;
    deduce3(std::move(rvref));
}

template <typename T1, typename T2>
struct ValuePair
{
    T1 fst;
    T2 snd;

    ValuePair(const T1& f, const T2& s) : fst{f}, snd{s}
    {}

    // template <typename T3>
    // ValuePair(T1 f, T2 s, T3 factor) : fst{f}, snd(s * factor)
    // {        
    // }
};

// deduction guides
template <typename T1, typename T2>
ValuePair(T1 f, T2 s) -> ValuePair<T1, T2>;

ValuePair(const char*, const char*) -> ValuePair<std::string, std::string>;

TEST_CASE("CTAD")
{
    ValuePair<int, double> vp1(1, 3.14);

    ValuePair vp2(1, 3.14); 
    static_assert(is_same_v<decltype(vp2), ValuePair<int, double>>);

    ValuePair vp3("text", "text"s); // ValuePair<const char*, std::string>        

    ValuePair vp4("abc", "def");
    static_assert(is_same_v<decltype(vp4), ValuePair<std::string, std::string>>);

    // ValuePair vp4(1, 2, 3.7);
    // REQUIRE(vp4.snd == 7);
}

TEST_CASE("special case for CTAD")
{
    vector vec1 = {1, 2, 3}; // vector<int>
    vector vec2{1, 2, 3}; // vector<int>

    vector vec3 = vec1; // vector<int>

    vector vec4{vec1}; // vector<int>
    vector vec5{vec1, vec1}; // vector<vector<int>>
}

template <typename T>
struct Value
{
    T value;
};

template <typename T>
Value(T) -> Value<T>;

TEST_CASE("CTAD + Aggregates")
{
    Value v1{10};
}

template <typename T>
struct Generic
{
    T value;

    using Type = common_type_t<T>;

    Generic(Type v)
        : value {v}
    {
    }
};

template <typename TArg>
Generic(TArg)->Generic<enable_if_t<is_arithmetic_v<TArg>, TArg>>;

TEST_CASE("disabling CTAD")
{
    Generic g = 10;

    //Generic g2 = "text";
}

TEST_CASE("CTAD in std")
{
    SECTION("pair")
    {
        pair p1(1, 3.14);
        pair p2("text", "text"s);
    }

    SECTION("tuple")
    {
        tuple t1{1, 3.14, "text"};
        tuple t2 = t1;
        tuple t3{t1}; // case with copy
        static_assert(is_same_v<decltype(t1), decltype(t2)>);
    }

    SECTION("optional")
    {
        optional o1 = 42; //optional<int>

        optional o2 = o1; //optional<int>
    }

    SECTION("smart_ptrs")
    {
        unique_ptr<int> ptr{new int(13)};

        shared_ptr sptr = std::move(ptr);

        weak_ptr wptr = sptr;
    }

    SECTION("function")
    {
        function f = [](int a, int b) { return a + b; };
    }

    SECTION("array")
    {
        array tab = {1, 2, 3, 4, 5};
        
        static_assert(size(tab) == 5);
    }
}

template <typename T, size_t N>
struct Array
{
    T items[N];

    constexpr size_t size() const
    {
        return N;
    }
};

template <typename Head, typename... Tail>
Array(Head, Tail...)->Array<enable_if_t<(is_same_v<Head, Tail> && ...), Head>, sizeof...(Tail) + 1>;