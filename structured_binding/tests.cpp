#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>
#include <set>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min, max;
        tie(min, max) = minmax_element(data.begin(), data.end());

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

template <typename Container>
auto calc_stats(const Container& data)
{    
    auto [min_pos, max_pos] = minmax_element(begin(data), end(data));

    double avg = accumulate(begin(data), end(data), 0.0) / size(data);

    return tuple(*min_pos, *max_pos, avg);
}

TEST_CASE("Before C++17")
{
    vector<int> data = {4, 42, 665, 1, 123, 13};

    SECTION("bad way")
    {
        auto stats = calc_stats(data);

        REQUIRE(std::get<0>(stats) == 1);
    }

    int min, max;
    double avg;

    tie(min, max, avg) = calc_stats(data);

    REQUIRE(min == 1);
    REQUIRE(max == 665);
    REQUIRE(avg == Approx(141.333));
}

TEST_CASE("Since C++17")
{
    int data[] = {4, 42, 665, 1, 123, 13};

    auto [min, max, avg] = calc_stats(data);

    REQUIRE(min == 1);
    REQUIRE(max == 665);
    REQUIRE(avg == Approx(141.333));
}

SCENARIO("vector")
{
    GIVEN("vector after creation")
    {
        std::vector<int> vec;    

        WHEN("push_back")
        {
            vec.push_back(1);
        
            THEN("increases size")
            {
                REQUIRE(vec.size() == 1);
            }

            SECTION("empty returns false")
            {
                REQUIRE(!vec.empty());
            }        
        }
    }
}

struct ErrorCode
{
    int errc;
    const char* m;
};

ErrorCode open_file(const char* filename)
{
    return {13, "Error#13"};
}

TEST_CASE("SB")
{    
    SECTION("array")
    {
        int tab[3] = {1, 2, 3};
        auto [x, y, z] = tab;
        REQUIRE(x == 1);
    }

    SECTION("std::pair")
    {
        std::set<int> numbers = {1, 2, 3};

        auto [pos, was_inserted] = numbers.insert(2);

        REQUIRE_FALSE(was_inserted);
    }

    SECTION("struct")
    {
        auto [error_code, message] = open_file("bad.name");        

        REQUIRE(error_code == 13);
    }
}

TEST_CASE("structured bindings use cases")
{
    SECTION("iteration over map")
    {
        std::map<int, string> dict = { {1, "one"}, {2, "two"} };

        for(const auto&[key, value] : dict)
        {
            std::cout << key << " - " << value << "\n";
        }
    }

    SECTION("init many vars")
    {
        list lst = {1, 2, 3, 4};
    
        for(auto [index, pos] = tuple(0, begin(lst)); pos != end(lst); ++pos, ++index)
        {
            cout << index << " - " << *pos << "\n";
        }
    }
}

enum Something
{
    some = 1, thing
};


const map<Something, string> something_desc = {{some, "some"}, {thing, "thing"}};

// step 1
template <>
struct std::tuple_size<Something>
{
    static constexpr size_t value = 2;
};


// step 2
template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = std::string;
};

// step 3
template <size_t Index>
decltype(auto) get(const Something&)
{
    if constexpr(Index == 0)
        static_cast<int>(sth);
    else
        something_desc.at(sth);
}

// template <>
// decltype(auto) get<0>(const Something& sth)
// {
//     return static_cast<int>(sth);
// }

// template <>
// decltype(auto) get<1>(const Something& sth)
// {
//     return something_desc.at(sth);
// }

TEST_CASE("tuple like protocol")
{
    Something sth = some;

    const auto [value, description] = sth;

    REQUIRE(value == 1);
    REQUIRE(description == "some"s);
}
