#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <string_view>
#include <array>
#include <optional>

#include "catch.hpp"

using namespace std;

TEST_CASE("string_view")
{
    SECTION("creation")
    {
        const char* ctext = "text";
        string_view sv1 = ctext;
        REQUIRE(sv1 == "text"sv);

        string str = "sting text";
        string_view sv2 = str;
        REQUIRE(sv2 == str);

        const char* words = "abc def ghi";
        string_view token1(words, 3);
        string_view token2(words + 4, 3);

        std::cout << token1 << "\n";
        std::cout << token2 << "\n";
        REQUIRE(token1 == "abc");
    }

    SECTION("string_view vs. strings & c-strings")
    {
        char text[] = {'a', 'b', 'c'};

        string_view sv{&text[0], 3};

        std::cout << sv << "\n";
        REQUIRE(sv == "abc");
    }
}

template <typename Container>
void print_all(const Container& container, string_view prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : container)
        cout << item << " ";
    cout << "]\n";    
}

TEST_CASE("print all")
{
    vector vec = {1, 2, 3, 4};

    print_all(vec, "vec");

    string prefix = "vector";
    print_all(vec, prefix);

    print_all(vec, "container"s);
}

string_view get_prefix(string_view text, size_t length)
{
    return {text.data(), length};
}

TEST_CASE("beware")
{
    //string_view evil_sv = "abc"s;

    string_view my_prefix = get_prefix("vector"s, 3);
    REQUIRE(my_prefix == "vec");
}

TEST_CASE("conversion")
{
    auto text = "abc"sv;

    string str(text);
}

namespace Cpp20
{
    template <typename Iterator, typename Predicate>
    constexpr Iterator find_if(Iterator first, Iterator last, Predicate pred)
    {
        for (Iterator it = first; it != last; ++it)
            if (pred(*it))
                return it;

        return last;
    }
}

template <auto N>
constexpr optional<string_view> get_id(const std::array<std::string_view, N>& data, string_view id)
{
    auto it = Cpp20::find_if(begin(data), end(data), [id](auto sv) { return sv == id; });

    if (it != end(data))
        return *it;

    return std::nullopt;
}

TEST_CASE("constexpr")
{
    constexpr string_view text = "abcdef"sv;
    constexpr string_view subtext(text.data() + 3, 3);
    static_assert(subtext == "def"sv);

    constexpr std::array ids = {"aa"sv, "bb"sv, "cc"sv, "dd"sv};
    constexpr optional opt_id = get_id(ids, "cc"sv);

    static_assert(opt_id.has_value());
    static_assert(opt_id.value() == "cc"sv);
}