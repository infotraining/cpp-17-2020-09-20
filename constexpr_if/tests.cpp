#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "catch.hpp"

using namespace std;

struct BigData
{
    int value;

    int calc()
    {
        return 2 * value;
    }
};

template <typename T>
struct SupportsAPI
{
    constexpr static bool value = false;
};

template <>
struct SupportsAPI<BigData>
{
    constexpr static bool value = true;
};

template <typename T>
inline constexpr bool SupportsAPI_v = SupportsAPI<T>::value;

namespace BeforeCpp17
{
    template <typename T>
    auto calculate(T obj) -> enable_if_t<SupportsAPI_v<T>, int>
    {
        return obj.calc();
    }

    template <typename T>
    auto calculate(T obj) -> enable_if_t<!SupportsAPI_v<T>, T>
    {
        return obj;
    }
}

template <typename T>
auto calculate(T obj)
{
    if constexpr(SupportsAPI_v<T>)
    {
        return obj.calc();
    }
    else
    {
        return obj;
    }
}

TEST_CASE("enable_if")
{
    int tab[] = {1, 2, 3};
    int target[3];
    std::copy(begin(tab), end(tab), begin(target));

    static_assert(SupportsAPI<int>::value == false);
    static_assert(SupportsAPI_v<BigData> == true);

    calculate(42);
    calculate(BigData{42});
}

template <typename T>
string convert_to_string(T value)
{
    if constexpr (is_arithmetic_v<T>)
        return to_string(value);
    else if constexpr (is_same_v<T, string>)
        return std::move(value);
    else
        return value;
}

TEST_CASE("convert to string")
{
    SECTION("from number")
    {
        string str = convert_to_string(42);

        REQUIRE(str == "42"s);
    }   

    SECTION("from string")
    {
        string str = convert_to_string("abc"s);

        REQUIRE(str == "abc"s);
    }    

    SECTION("from c-string")
    {
        string str = convert_to_string("abc");

        REQUIRE(str == "abc"s);
    }
}

template <typename T, size_t N>
auto process_array(T (&data)[N])
{
    if constexpr (N <= 255)
    {
        cout << "Processing small array\n";
        std::array<T, N> result;
        copy(begin(data), end(data), begin(result));
        return result;
    }
    else
    {
        cout << "Processing large array\n";
        std::vector<T> result(N);
        copy(begin(data), end(data), begin(result));
        return result;
    }
}

TEST_CASE("processing arrays")
{
    int small_buffer[16] = {1, 2, 3, 4};
    auto backup1 = process_array(small_buffer);
    static_assert(is_same_v<decltype(backup1), std::array<int, 16>>);

    int large_buffer[1024];
    auto backup2 = process_array(large_buffer);
    static_assert(is_same_v<decltype(backup2), std::vector<int>>);
}

namespace BeforeCpp17
{
    void print()
    {
        cout << "\n";
    }

    template <typename Head, typename... Tail>
    void print(Head h, Tail... tail)
    {
        std::cout << h << " ";
        print(tail...);
    }    
}

template <typename Head, typename... Tail>
void print(Head h, Tail... tail)
{
    std::cout << h << " ";

    if constexpr (sizeof...(tail) > 0)
        print(tail...);
    else
    {
        cout << "\n";
    }    
}    

TEST_CASE("variadic templates")
{
    print(1, 3.14, "test"s);
}