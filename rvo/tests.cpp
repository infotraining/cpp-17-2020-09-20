#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

std::vector<int> create_vector_rvo(size_t n) // since C++17 rvo is mandatory
{
    return vector<int>(n, -1); // prvalue
}

std::vector<int> create_vector_nrvo(size_t n)
{
    vector<int> vec(n);
    std::iota(begin(vec), end(vec), 0);
    return vec; // lvalue
}

TEST_CASE("rvo")
{
    vector<int> vec1 = create_vector_rvo(1'000'000); // direct initialization with prvalue from return
    vector<int> vec2 = create_vector_nrvo(1'000); // may by copy or move
}

struct CopyMoveDisabled
{
    vector<int> data;

    CopyMoveDisabled(std::initializer_list<int> lst)
        : data {lst}
    {
    }

    CopyMoveDisabled(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled& operator=(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled(CopyMoveDisabled&&) = delete;
    CopyMoveDisabled& operator=(CopyMoveDisabled&&) = delete;
    ~CopyMoveDisabled() = default;
};

CopyMoveDisabled create_impossible()
{
    return CopyMoveDisabled{1, 2, 3};
}

void use(CopyMoveDisabled arg)
{
    cout << "Using object: " << arg.data.size() << "\n";
}

TEST_CASE("copy & move disabled")
{
    CopyMoveDisabled cmd = create_impossible();

    use(CopyMoveDisabled({1, 2, 4}));
    use(create_impossible());

    // CopyMoveDisabled arg{1, 2, 3};
    // use(std::move(arg));
}

struct X
{
    int v;

    X() : v{-1}
    {}

    X(int v) : v{v}
    {}

    X(std::initializer_list<int> lst)
    {
        v = lst.size();
    }
};

TEST_CASE("auto + {}")
{
    auto lst = {10}; // std::initializer_list
    auto n{10}; // int
    auto v(10);

    auto x = X{};

    REQUIRE(x.v == 0);
}